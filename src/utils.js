

export function getUrlParameter(uri){
    return uri.search.slice(1,uri.search.length)
        .split('&')
        .reduce((acc,cur)=>{
            let arr=cur.split("=");
            acc[arr[0]]=arr[1];
            return acc;},[])
}
export function getRandomID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    let r = Math.random() * 16 | 0,
    v = c === 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
    });
}