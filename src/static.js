import data from './DAL/reddit.json';
export const urlReddit2='https://www.reddit.com/api/v1/authorize?client_id=6ETZGVmrcrR7jQ&response_type=code&state=rwtetyyt&redirect_uri=http://localhost:3000/&duration=permanent&scope=read';
export const ACCESS_DENIED = 'access_denied';
export const UNSUPPORTED_RESPONSE_TYPE = 'unsupported_response_type';
export const INVALID_SCOPE ='invalid_scope';
export const INVALIDE_REQUEST = 'invalid_request';
const clientID = data.clientID;
const responseType='code';
export const urlState = '3dfbed32-4021-4039-9988-9a49471898fa';
export const uriRedirect = 'http://localhost:3000/';
export const urlReddit3=`https://www.reddit.com/api/v1/authorize?client_id=${clientID}&response_type=${responseType}&state=${urlState}&redirect_uri=${uriRedirect}&duration=permanent&scope=read`
export const result = Object.freeze({'inProcess':0,'success':1,'failure':2,'newRequest':3})
export const mode = Object.freeze({'requestAccess':0,'tokenRefresh':1,'accessGranted':2,'requestToken':3})