import React,{Component} from 'react';
//import RedditElement from './redditElement.js'
import {ClimbingBoxLoader} from 'react-spinners';
import {Table,TableBody,TableHeader,TableHeaderColumn,TableRow,TableRowColumn} from 'material-ui/Table';
import { result } from "../static.js";
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import ActionYoutubeSearchedFor from 'material-ui/svg-icons/action/youtube-searched-for';
import ActionAutorenew from 'material-ui/svg-icons/action/autorenew';
import PropTypes from 'prop-types';
import './reddit.css';

const styles = {
    button: {
      margin: 12,
    },
  };

class RedditList extends Component{
    render(){
        if (this.props.result === result.inProcess){
            return (
                <div id='wait'>
                    <p>Please wait while we retrieve your data...</p>
                    <ClimbingBoxLoader />
                </div>
            );
        }else{
            return(
                <div>
                    <form onSubmit={this.props.fetchData}>
                        <fieldset>
                            <legend>Ready to fetch data</legend>
                            <TextField floatingLabelText="Subreddit" id="txtSubreddit" 
                                       value={this.props.subreddit} 
                                       onChange={this.props.onChange}/>
                            <RaisedButton
                                label="Fetch"
                                labelPosition="before"
                                style={styles.button}
                                icon = {<ActionYoutubeSearchedFor />}
                                type="submit"
                                >
                            </RaisedButton>
                        </fieldset>
                    </form>
                    <br/>
                    <RaisedButton onClick={this.props.refreshToken} 
                                  label="Renew token" 
                                  labelPosition="before" 
                                  style={styles.button} 
                                  icon={<ActionAutorenew />}
                                  >
                    </RaisedButton>

                    {this.props.result===result.failure &&
                        <p>{this.props.data[0].error} {this.props.data[0].message}</p>
                    }
                    {   this.props.result === result.success &&
                        this.props.data.length > 1 
                        &&
                        <Table>
                            <TableHeader>
                                <TableRow>
                                    {Object.keys(this.props.data[0]).map((header,i)=><TableHeaderColumn key={i}>{header}</TableHeaderColumn>)}
                                </TableRow>
                            </TableHeader>
                            <TableBody>
                                {this.props.data.map((elem,i)=>
                                    <TableRow key={i}>
                                        {Object.keys(elem).map((key,i)=>
                                            <TableRowColumn key={i}>{key==='path'? <a target="_blank" href={`https://www.reddit.com${elem[key]}`}> {elem[key]}</a> : elem[key]}</TableRowColumn>)
                                        }
                                    </TableRow>
                                )}
                            </TableBody>
                        </Table>
                        }
                </div>
            );
        }
       
    }
}


RedditList.propTypes = {
    data:PropTypes.array,
    result:PropTypes.number,
    mode : PropTypes.number,
    fetchData : PropTypes.func,
    refreshToken : PropTypes.func,
    subreddit : PropTypes.string,
    onChange : PropTypes.func
};
export default RedditList;