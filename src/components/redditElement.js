import React,{Component} from 'react';
import {TableRow,TableRowColumn} from 'material-ui/Table';
class RedditElement extends Component{
    render(){
        return(
           <TableRow>
               {Object.keys(this.props.data).map((key,i)=>
                    <TableRowColumn>{this.props.data[key]}</TableRowColumn>
                    )}
           </TableRow>
        );
    }
}

export default RedditElement;