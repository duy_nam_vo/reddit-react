import React,{Component} from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import ActionLaunch from 'material-ui/svg-icons/action/launch';
import ActionBackup from 'material-ui/svg-icons/action/backup';
import {ACCESS_DENIED
       ,INVALID_SCOPE
       ,INVALIDE_REQUEST
       ,UNSUPPORTED_RESPONSE_TYPE
       ,mode
       ,result} from '../static.js';
       const styles = {
        button: {
          margin: 12,
        }}
class AccessRequest extends Component{
    render(){
        return(
            <div>
            {this.props.mode === mode.requestAccess && 
            <   RaisedButton label="Request access" style={styles.button} icon={<ActionLaunch/>} onClick={this.props.requestAccess} ></RaisedButton>
            // <button type="button" onClick={this.props.requestAccess}>Request access</button>
            }
            {this.props.mode === mode.requestToken && 
                <RaisedButton label="Get token" style={styles.button} icon={<ActionBackup/>} onClick={this.props.getToken} ></RaisedButton>

            }
            {(this.props.accessStatue === ACCESS_DENIED ||
              this.props.accessStatue === INVALID_SCOPE ||
              this.props.accessStatue === INVALIDE_REQUEST ||
              this.props.accessStatue === UNSUPPORTED_RESPONSE_TYPE) && <p>{this.props.accessStatue}</p>}
            {<p>{this.props.message}</p>}
            </div>
        );
    }
}

export default AccessRequest;