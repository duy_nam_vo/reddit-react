import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    Table,
    TableBody,
    TableRow,
    TableRowColumn,
  } from 'material-ui/Table';
import './table.css';
import Toggle from 'material-ui/Toggle';
  



export default class Status extends Component {
  render() {
      console.log(this.props.statusToken)
    return (
        <div className='l-status'> 
            <Toggle className='l-toggle' label='Show status' defaultToggled={false} onToggle={this.props.toggleStatus}/>
            {this.props.showStatus === true && 

                <Table className='l-table'>
                <TableBody displayRowCheckbox={false}>
                    {Object.keys(this.props.statusToken).map((tk,i)=>
                        <TableRow key={i}>
                            <TableRowColumn>
                                {tk}
                            </TableRowColumn>
                            <TableRowColumn>
                                {this.props.statusToken[tk]}
                            </TableRowColumn>
                        </TableRow>
                    )}
                  
                    
                </TableBody>
            </Table>
            }
            
    </div>
    )
  }
}

Status.propTypes={
 showStatus: PropTypes.bool.isRequired,
 statusToken: PropTypes.object
}
