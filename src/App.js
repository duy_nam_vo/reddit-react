import React, { Component } from 'react';
import logo from './reddit-2.svg';
import './App.css';
import {urlReddit3,INVALIDE_REQUEST} from './static.js';
import {getUrlParameter} from './utils.js';
import {urlState,mode,result} from './static.js';
import redditAccess from './DAL/reddit';
import AccessRequest from './components/accessRequest.js';
import RedditList from './components/redditList.js';
import Status from './components/status.js';

/* var m = new metacriticAccess(acc.url,acc.key)
m.getData('movie','black-swan'); */
class App extends Component {
  constructor(props){
    super(props);
    this.state={
        accessStatue:'',
        redditState:'',
        message:'First request access to reddit',
        data:[],
        subreddit:'',
        mode:mode.requestAccess,
        result:result.newRequest,
        showStatus:false,
        statusToken:{},
    }
    this.redditCode= '';
    this.requestAccess = this.requestAccess.bind(this);
    this.getToken = this.getToken.bind(this);
    this.fetchData = this.fetchData.bind(this);
    this.refreshToken = this.refreshToken.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onAccessCallback = this.onAccessCallback.bind(this);
    this.toggleStatus = this.toggleStatus.bind(this);
  }

  toggleStatus(){
    this.setState({showStatus:!this.state.showStatus})
   
  }
  getToken(){
    var red = new redditAccess({redditCode : this.redditCode,fnCallback:this.onAccessCallback});
    red.fetchToken();
  }
  onChange(event){
    this.setState({subreddit:event.target.value})
  }

  fetchData(event){
    event.preventDefault();
    var red = new redditAccess({fnCallback:this.onAccessCallback});
    red.fetchData(this.state.subreddit === "" ? 'reactjs' : this.state.subreddit);
  }
  refreshToken(){
      var red = new redditAccess({fnCallback:this.onAccessCallback});
      red.refreshToken();
  }
  

  onAccessCallback(mode,result,message='',data=undefined){
    console.log(result)
    let arr = data === undefined ? [] : Array.isArray(data) === true ? data : Array(1).fill(data)
    this.setState({mode:mode
                  ,result:result
                  ,message:message
                  ,data:arr})
  }
  requestAccess(){
    window.location.href=urlReddit3;
  }

  componentDidMount(){
    var red = new redditAccess({fnCallback:this.onAccessCallback});
    if (red.isTokenFetched()){
      let tokens = red.getTokens();
      this.setState({mode:mode.accessGranted,result:result.newRequest,message:'Ready to fetch data',statusToken:tokens})
    }
    var param = getUrlParameter(window.location);
    if('code' in param && 'state' in param){
      if (param.state !== urlState){
        this.setState({accessStatue:INVALIDE_REQUEST,redditState:param['state']})
      }else{
        this.redditCode=param.code;
        this.setState({mode:mode.requestToken,message:'Request token'});
      }
    }
    if (param[0] !== ""){
      this.setState({accessStatue:param['error'],redditState:param['state']})
    }
   
  }
 
  render() {
    let display;
    console.log(this.state)
    switch (this.state.mode){
      case mode.accessGranted:
        display=<RedditList {...this.state}
                            fetchData={this.fetchData}
                            refreshToken = {this.refreshToken}
                            onChange = {this.onChange}
                  />
        break;
      
      case mode.requestToken:
      case mode.requestAccess:
      case mode.tokenRefresh:
      default:
        display = <AccessRequest {...this.state}
                                requestAccess={this.requestAccess}
                                getToken={this.getToken}
                                />
        break;
    }
    return (
     
      <div className="App">

        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Reddit client</h1>
        </header>
       
          <Status {...this.state} toggleStatus={this.toggleStatus}/>
          
        {display}

      </div>
    );
  }
} 

export default App;
