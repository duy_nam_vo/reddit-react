export class metacriticAccess {
    constructor(url,key){
        this.key = key;
        this.url = url;
    }
    getData(searchType,query){
        console.log(this.key)
        fetch(`${this.url}/${searchType}/${query}`,{
            method:'GET',
            headers: new Headers({
                "X-Mashape-Key":this.key,
                "Accept":"application/json"
            })
        }).then(res=>res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response));
    }
}

