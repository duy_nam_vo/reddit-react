export var getToken = (prefix) => (tokenName) => {
    return window.localStorage[`${prefix}_${tokenName}`]
    // return {
    //     'access_token' : window.localStorage[`${prefix}_access_token`],
    //     'refresh_token': window.localStorage[`${prefix}_refresh_token`],
    //     'scope'        : window.localStorage[`${prefix}_scope`],
    //     'token_type'   : window.localStorage[`${prefix}_token_type`]
    // }
}
export var setToken = (prefix) => (tokenName, tokenValue) => {
    window.localStorage[`${prefix}_${tokenName}`] = tokenValue
}

export var setAllTokens = (prefix) =>(tokens)=>{
    Object.keys(tokens).map((token)=>{
        window.localStorage[`${prefix}_${token}`] = tokens[token]
    })
}

export var getAllTokens = (prefix) =>()=>{
    let d = window.localStorage;
    let tokensName = Object.keys(d).filter((elem)=>elem.startsWith(prefix))
    return tokensName.reduce((acc,cur)=>{acc[cur]=d[cur];return acc},{})
}