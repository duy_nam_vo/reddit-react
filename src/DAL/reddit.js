import data from './reddit.json';
import {uriRedirect,mode,result} from '../static.js';
import * as dataStore from "./dataStore.js";
const redditApiUri = 'https://oauth.reddit.com/api'
const userAgent = 'javascript:localhost:v1 (by /u/duynamvo)';
const uriToken = 'https://www.reddit.com/api/v1/access_token';



class redditAccess{
    constructor(options){
        
        this._redditCode = options.redditCode === undefined ? "" : options.redditCode;
        this._fnCallback = options.fnCallback !== undefined && typeof options.fnCallback === 'function' 
                        ? options.fnCallback 
                        : (mode,result,message,data)=>{console.log(message)};

        this._encodedAuth        = btoa(`${data.clientID}:${data.secretID}`);
        this._appPrefix          = 'reddit';
        this._setRedditToken     = dataStore.setToken(this._appPrefix);
        this._getRedditToken     = dataStore.getToken(this._appPrefix);
        this._setRedditAllTokens = dataStore.setAllTokens(this._appPrefix);
        this._getRedditAllTokens = dataStore.getAllTokens(this._appPrefix);
    }
    getTokens(){
        if (this.isTokenFetched()){
            return this._getRedditAllTokens();
        }
    }
    isTokenFetched(){
        if (this._getRedditToken('access_token') !== undefined && this._getRedditToken('access_token') !== ""){
            return true;
        }else{
            return false;
        }
       
    }
    fetchToken(){
        if (this.isTokenFetched()){
            return true;
        }
        var form = new FormData()
        form.append('code', this._redditCode)
        form.append('grant_type', "authorization_code")
        form.append('redirect_uri', uriRedirect)
        this._fnCallback(mode.requestToken,result.inProcess,'Fetching token')
       
        fetch(uriToken,{
            method:'POST',
            headers: new Headers({
                'Authorization':`Basic ${this._encodedAuth}`,
                'User-Agent':userAgent
            }),
            body:form
        })
        .then(res=>{
                    if (!res.ok){
                        throw Error(res.statusText);
                    }else{
                        return res.json()
                    }
                })
        .catch(error=>{
            this._fnCallback(mode.requestAccess,result.failure,'An error happened')
           
        })
        .then(response=>{
            if (response !== undefined){
                if ('error' in response){
                    this._fnCallback(mode.requestAccess,result.failure,'An error happened')
                }else{
                    this._setRedditAllTokens(response)
                    // this._setRedditToken('access_token',response.access_token)
                    // this._setRedditToken('refresh_token',response.refresh_token)
                    // this._setRedditToken('expires_in',response.expires_in)
                    // this._setRedditToken('scope',response.scope)
                    // this._setRedditToken('token_type',response.token_type)
                    this._fnCallback(mode.accessGranted,result.newRequest,'token successfully retrieved')
                }
            }
        })
    }
    refreshToken(){
        this._fnCallback(mode.tokenRefresh,result.inProcess,'refreshing token')
        
        let refreshToken = this._getRedditToken('refresh_token') 
        var form = new FormData()
        form.append('grant_type', "refresh_token")
        form.append('refresh_token', refreshToken)
        fetch(uriToken,{
            method:'POST',
            headers: new Headers({
                'Authorization':`Basic ${this._encodedAuth}`,
                'User-Agent':userAgent
            }),
            body:form
        }).then(res=>{
            if (!res.ok){
                throw Error(res.statusText);
            }else{
                return res.json()

            }
        })
        .catch(error=>{
            this._fnCallback(mode.tokenRefresh,result.failure,'An error happened')
            
        })
        .then(response=>{
            if (response !== undefined){
                if ('error' in response){
                    this._fnCallback(mode.tokenRefresh,result.failure,response.error)
                }else{
                    // this._setRedditToken('access_token',response.access_token)
                    // this._setRedditToken('refresh_token',response.refresh_token)
                    // this._setRedditToken('expires_in',response.expires_in)
                    // this._setRedditToken('scope',response.scope)
                    // this._setRedditToken('token_type',response.token_type)
                    this._setRedditAllTokens(response)
                    this._fnCallback(mode.accessGranted,result.newRequest,'token successfully refreshed')
                }
            }
        })
    }
    
    fetchData(subreddit='javascript'){
        console.log('fetch data');
        this._fnCallback(mode.accessGranted,result.inProcess,'Processing request ...')
        let token = this._getRedditToken('access_token')
        fetch(`${redditApiUri}/subreddits_by_topic/?query=${subreddit}`,{
            method:'GET',
            headers:new Headers({
                'Authorization': `bearer ${token}`
            })
        }).then(response=>{
            return response.json();
        })
        .catch(error=>console.error(error))
        .then(response=> {
            if ('error' in response ){
                this._fnCallback(mode.accessGranted,result.failure,'Error processing data fetch',response)
            }else {
                this._fnCallback(mode.accessGranted,result.success,'Successfully retrieved data',response)
            }
           
        })
    }
}

export default redditAccess;